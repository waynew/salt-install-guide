.. _downloads:

=========
Downloads
=========

.. sidebar:: **Supported Salt releases**

    |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    **See also**

    * :ref:`salt-supported-operating-systems`
    * :ref:`salt-version-support-lifecycle`
    * :ref:`salt-python-version-support`


Welcome to the Salt download page!

* Salt packages are hosted on `repo.saltproject.io <https://repo.saltproject.io/>`_.
* Unsupported versions can be found in the `PY2 archive repository <https://archive.repo.saltproject.io/salt/py2/>`__
  and the `PY3 archive repository <https://archive.repo.saltproject.io/salt/py3/>`_.

The following sections provide links to the repository for each operating
system as well as links to the installation instructions for each operating
system.

.. Tip::
    As a security best practice, always verify that you have downloaded Salt
    from the authorized site by comparing the SHA/MD5 of the package or by
    verifying the package URL is an official Salt repository.


AIX
===
The AIX native minion package is a tarball containing an installation and
removal script and an AIX bff package.

.. grid:: 2

    .. grid-item-card:: Browse the repo for AIX packages
        :link: https://repo.saltproject.io/salt/py3/aix/

        :bdg-danger:`AIX`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: AIX install guide
        :link: install-aix
        :link-type: ref

        :bdg-info:`Install Salt on AIX`


.. warning::

    There are separate builds for AIX v7.1 and AIX v7.2.


Amazon Linux 2
==============
Install the Amazon Linux 2 package using a Linux package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Amazon Linux 2 packages
        :link: https://repo.saltproject.io/py3/amazon/

        :bdg-danger:`Amazon Linux 2`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: Amazon Linux 2 install guide
        :link: install-amazon
        :link-type: ref

        :bdg-info:`Install Salt on Amazon Linux 2`


Arista
======
The Arista native minion package is a SWIX file.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Arista packages
        :link: https://repo.saltproject.io/salt/py3/arista/

        :bdg-danger:`Arista`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: Arista install guide
        :link: install-arista
        :link-type: ref

        :bdg-info:`Install Salt on Arista`


CentOS
======
Install the CentOS package using a yum package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for CentOS packages
        :link: https://repo.saltproject.io/py3/redhat/

        :bdg-danger:`CentOS`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: CentOS install guide
        :link: install-centos
        :link-type: ref

        :bdg-info:`Install Salt on CentOS`


Debian
======
Install the Debian package using an apt package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Debian packages
        :link: https://repo.saltproject.io/py3/debian/

        :bdg-danger:`Debian`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: Debian install guide
        :link: install-debian
        :link-type: ref

        :bdg-info:`Install Salt on Debian`



Fedora
======
Install the Fedora package from the Fedora package manager.

.. card:: Fedora install guide
    :link: install-fedora
    :link-type: ref
    :width: 50%

    :bdg-info:`Install Salt on Fedora`


Juniper
=======
The Juniper native minion| package is a tarball (.tgz) file.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Juniper packages
        :link: https://repo.saltproject.io/salt/py3/juniper/

        :bdg-danger:`Juniper`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: Juniper install guide
        :link: install-juniper
        :link-type: ref

        :bdg-info:`Install Salt on Juniper`


macOS
=====
Download the macOS package and run the file to install Salt. See the
:ref:`install-macos` install guide for more information.

.. include:: _includes/macos-downloads.rst

.. grid:: 2

    .. grid-item-card:: Browse the repo for macOS packages
        :link: https://repo.saltproject.io/osx/

        :bdg-danger:`macOS`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: macOS install guide
        :link: install-macos
        :link-type: ref

        :bdg-info:`Install Salt on macOS`


Photon OS
=========
Install the Photon OS package using a yum package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Photon OS packages
        :link: https://repo.saltproject.io/py3/

        :bdg-danger:`Photon OS`
        :bdg-secondary:`Python3`
        |release-badge|

    .. grid-item-card:: Photon OS install guide
        :link: install-photonos
        :link-type: ref

        :bdg-info:`Install Salt on Photon OS`


Raspbian
========
Use the same download and installation instructions as `Debian`_.


RedHat
======
Install the RedHat package using a yum package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for RedHat (RHEL) packages
        :link: https://repo.saltproject.io/py3/redhat/

        :bdg-danger:`RedHat`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: RedHat install guide
        :link: install-rhel
        :link-type: ref

        :bdg-info:`Install Salt on RedHat`


Solaris
=======
The Solaris native minion package is available in the ``p5p`` format or as a
tarball (.tgz) file.


.. grid:: 2

    .. grid-item-card:: Browse the repo for Solaris packages
        :link: https://repo.saltproject.io/salt/py3/solaris/

        :bdg-danger:`Solaris`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: Solaris install guide
        :link: install-solaris
        :link-type: ref

        :bdg-info:`Install Salt on Solaris`


Ubuntu
======
Install the Ubuntu package using an apt package manager.

.. grid:: 2

    .. grid-item-card:: Browse the repo for Ubuntu packages
        :link: https://repo.saltproject.io/py3/ubuntu/

        :bdg-danger:`Ubuntu`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: Ubuntu install guide
        :link: install-ubuntu
        :link-type: ref

        :bdg-info:`Install Salt on Ubuntu`


Windows
=======
Download the Windows package and run the file to install Salt. See the
:ref:`install-windows` install guide for more information.

.. include:: _includes/windows-downloads.rst

.. grid:: 2

    .. grid-item-card:: Browse the repo for Windows packages
        :link: https://repo.saltproject.io/windows/

        :bdg-danger:`Windows`
        :bdg-secondary:`Python3`
        |release-badge| |supported-release-1-badge| |supported-release-2-badge|

    .. grid-item-card:: Windows install guide
        :link: install-windows
        :link-type: ref

        :bdg-info:`Install Salt on Windows`
