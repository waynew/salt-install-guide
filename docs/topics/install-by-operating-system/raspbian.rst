.. _install-raspbian:

========
Raspbian
========

For Raspbian operating systems, use the instructions for installing Salt on
:ref:`install-debian`. Raspbian operating systems directly parallel Debian
version numbers.
