Before you start
================
Before installing Salt on your operating system, ensure that you first:

* :ref:`check-system-requirements`
* :ref:`check-network-ports`
* :ref:`install-salt-dependencies`
