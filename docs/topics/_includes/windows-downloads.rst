.. list-table::
  :widths: 10 10 10 40 10 10
  :header-rows: 1
  :class: checkmarks

  * - OS
    - Arch
    - File Type
    - Download
    - MD5
    - SHA256

  * - Windows
    - AMD64
    - msi
    - `Salt-Minion-3004.1-1-Py3-AMD64.msi <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-AMD64.msi>`_
    - `MD5 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-AMD64.msi.md5>`__
    - `SHA256 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-AMD64.msi.sha256>`__

  * - Windows
    - AMD64
    - exe
    - `Salt-Minion-3004.1-1-Py3-AMD64-Setup.exe <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-AMD64-Setup.exe>`_
    - `MD5 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-AMD64-Setup.exe.md5>`__
    - `SHA256 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-AMD64-Setup.exe.sha256>`__

  * - Windows
    - x86
    - msi
    - `Salt-Minion-3004.1-1-Py3-x86.msi <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-x86.msi>`_
    - `MD5 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-x86.msi.md5>`__
    - `SHA256 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-x86.msi.sha256>`__

  * - Windows
    - x86
    - exe
    - `Salt-Minion-3004.1-1-Py3-x86-Setup.exe <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-x86-Setup.exe>`_
    - `MD5 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-x86-Setup.exe.md5>`__
    - `SHA256 <https://repo.saltproject.io/windows/Salt-Minion-3004.1-1-Py3-x86-Setup.exe.sha256>`__
