.. list-table::
  :widths: 10 10 10 40 10 10
  :header-rows: 1
  :class: checkmarks

  * - OS
    - Arch
    - File Type
    - Download
    - MD5
    - SHA256

  * - macOS
    - x86_64
    - pkg
    - `salt-3004.1-1-py3-x86_64.pkg <https://repo.saltproject.io/osx/salt-3004.1-1-py3-x86_64.pkg>`_
    - `MD5 <https://repo.saltproject.io/osx/salt-3004.1-1-py3-x86_64.pkg.md5>`_
    - `SHA265 <https://repo.saltproject.io/osx/salt-3004.1-1-py3-x86_64.pkg.sha256>`_
