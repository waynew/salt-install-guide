.. Warning::

   The Salt Project will support the classic, non-onedir packaging system until
   the 3006 release. After that time, onedir will become the standard packaging
   system for the Salt Project. The following instructions explain how to
   install Salt using the classic packaging system, but you should consider
   upgrading to onedir soon. See :ref:`upgrade-to-onedir` for more information.
