.. _salt-supported-operating-systems:

===========================
Supported operating systems
===========================

Together with the :ref:`salt-version-support-lifecycle` guidelines, this
document is intended to clearly define how long a particular version of Salt
will receive official packages, testing, and technical support from the Salt
Project.

.. include:: _includes/supported-os-concepts.rst


Overview of supported operating systems
=======================================

.. list-table::
  :widths: 30 10 10 10 15 25
  :align: center
  :header-rows: 1
  :stub-columns: 1
  :class: slim

  * -
    - `Master supported`_
    - `Minion supported`_
    - `Full support`_
    - `Reasonable-effort support`_
    - Testing support

  * - `AIX`_ 7.1
    -
    - Yes
    - Yes
    -
    - Package only

  * - `AIX`_ 7.2
    -
    - Yes
    - Yes
    -
    - Package only

  * - `AIX`_ 7.3
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Amazon Linux`_ 2
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Arch`_ (latest)
    - Yes
    - Yes
    -
    - Yes
    - Automated only

  * - `Arista`_ 4.18
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Arista`_ 4.23
    -
    - Yes
    - Yes
    -
    - Package only

  * - `CentOS`_ 7
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `CentOS`_ 8 Streaming
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `CentOS`_ 9 Streaming
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `Debian`_ 10
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `Debian`_ 11
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `Fedora`_ 35
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `Fedora`_ 36
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `Fedora`_ 37
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `FreeBSD`_ 11
    - Yes
    - Yes
    -
    - Yes
    - Package only

  * - `FreeBSD`_ 12.2
    - Yes
    - Yes
    -
    - Yes
    - Package only

  * - `FreeBSD`_ 13
    - Yes
    - Yes
    -
    - Yes
    - Package only

  * - `Juniper`_ (JunOS)
    -
    - Yes
    - Yes
    -
    - Package only

  * - `macOS`_ 10.15
    -
    - Yes
    - Yes
    -
    -

  * - `macOS`_ 11
    -
    - Yes
    - Yes
    -
    -

  * - `macOS`_ 12
    -
    - Yes
    - Yes
    -
    -

  * - `openSUSE`_ Leap 42.3
    - Yes
    - Yes
    -
    - Yes
    - Automated only

  * - `openSUSE`_ 15
    - Yes
    - Yes
    -
    - Yes
    -

  * - `Oracle Linux`_ 7, 8
    - Yes
    - Yes
    - Yes
    -
    - NONE [#f1]_

  * - `Photon OS`_ 3
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `Raspberry Pi OS`_ 9 (Stretch)
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `Raspberry Pi OS`_ 10 (Buster)
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `Raspberry Pi OS`_ 11 (Bullseye)
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `RedHat`_ 7
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `RedHat`_ 8
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `RedHat`_ 9
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `SLES`_ 12
    - Yes
    - Yes
    - Yes
    -
    -

  * - `SLES`_ 12 SP4
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `SLES`_ 15
    - Yes
    - Yes
    - Yes
    -
    - Package only

  * - `SmartOS`_ (latest)
    - Yes
    - Yes
    -
    - Yes
    - Package only

  * - `Solaris`_ 10
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Solaris`_ 11.4 and greater
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Ubuntu`_ 18.04
    - Yes
    - Yes
    - Yes
    -
    -

  * - `Ubuntu`_ 20.04
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `Ubuntu`_ 22.04
    - Yes
    - Yes
    - Yes
    -
    - Automated, manual

  * - `Windows`_ Desktop 8.1
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Windows`_ Desktop 10 
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Windows`_ Desktop 11
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Windows`_ 2012
    -
    - Yes
    - Yes
    -
    -

  * - `Windows`_ 2012 R2
    -
    - Yes
    - Yes
    -
    -

  * - `Windows`_ 2016
    -
    - Yes
    - Yes
    -
    - Package only

  * - `Windows`_ 2019 
    -
    - Yes
    - Yes
    -
    -

  * - `Windows`_ 2022
    -
    - Yes
    - Yes
    -
    -


.. [#f1] Use RedHat or CentOS packages instead.


Master supported
----------------
Master supported is defined as:

* The `salt-master` service can run on this operating system.
* A node running this operating system can act as the Salt master, which means
  it can send commands and communicate with connected Salt minions.


Minion supported
----------------
Minion supported is defined as:

* The `salt-minion` service can run on this operating system.
* A node running this operating system can act a Salt minion, which means this
  system or device can be managed by a Salt master.


Full support
------------
Full support is defined as:

* Packages and all required dependencies created by Salt Project or official upstream packager.
* Packages hosted by the Salt Project.
* Tested by the Salt Project.
* The Salt Project provides full technical support for VMware customers.
* The Salt Project will fix bugs for VMware customers.


Reasonable-effort support
-------------------------
Reasonable-effort support is defined as:

* Packages created and hosted by the Salt Project community.
* Some testing done by the Salt Project.
* The Salt Project provides best-effort technical support for VMware customers.
* The Salt Project may fix bugs for VMware customers.


Full support policy by operating system
=======================================
This section outlines the general support and package creation policy for each
operating system that is listed as having full support by the Salt Project.
These guidelines are intended to help you understand how long a particular
operating system will receive official packages, testing, and technical support.


.. _aix-supported:

AIX
---
The Salt Project supports:

  * AIX version 7.1 as of May 2017.
  * AIX version 7.2 as of November 2021.
  * AIX version 7.3 as of May 2022.

The following systems are supported:

.. list-table::
   :header-rows: 1

   * - Device
     - Supported Firmware
     - Supported Salt versions
   * - AIX 7.1 and greater
     - Power 7 and greater
     - Salt v3002.1 and greater (Python 2)
   * - AIX 7.1 and greater
     - Power 8 and greater
     - Salt v3003.1 and greater
   * - AIX 7.2 and greater
     - Power 8 and greater
     - Salt v3004 and greater


.. Warning::
    Be aware that AIX utilities may not be the same as a standard Linux
    environment. For example, the version of ``tar`` does not support ``-z`` in
    AIX.

    Salt ``3003.1`` releases, and newer, will only work on **Power 8 or greater
    processors.**


Amazon Linux
------------
Amazon Linux 2 is supported.


Arch
----
Latest version of Arch.


.. _arista-supported:

Arista
------
The following Arista systems are supported:

.. list-table::
   :header-rows: 1

   * - Device
     - Supported Firmware
   * - Arista 32-bit EOS
     - Versions 4.18 and greater
   * - Arista 64-bit EOS
     - Versions 4.23 and greater

The Arista native minion already contains Arista’s `pyeapi
<https://github.com/arista-eosplus/pyeapi>`_ software, as well as `Napalm
<https://github.com/napalm-automation/napalm>`_ and all of its dependencies.


CentOS
------
CentOS versions through Production Phase 3 Support. Versions in ELS are not
supported by the Salt Project.


Debian
------
Debian stable, oldstable, and oldoldstable versions.

https://wiki.debian.org/DebianReleases


Fedora
------
Fedora version support mirrors that of the upstream maintainer.


RedHat
------
RedHat versions through Production Phase 3 Support. Versions in ELS are not
supported by the Salt Project.

https://access.redhat.com/support/policy/updates/errata 


.. _juniper-supported:

Juniper
-------
The following systems are supported:

.. list-table::
   :header-rows: 1

   * - Device
     - Supported Firmware
   * - QFX Series Switches
     - **17.3R2-S6** and greater
   * - MX Series Routers
     - **17.4R2-S6** and greater


.. Note::
    During development of the native minion for Juniper, an issue was found
    on Juniper software which required a fix: Juniper PR 1515432 : REST-API
    PUT POST RPC call failing, which has been fixed in 17.4R2-S12 and 17.3R3-S9.
    Use these versions or higher for use with the native minion.


macOS
-----
The latest three versions of macOS.


Oracle Linux
------------
Oracle Linux versions 7 and 8 as of July 2016.

https://www.oracle.com/technetwork/server-storage/linux/overview/index.html


Photon OS
---------
Photon OS 3.0 as of the 3005 (Phosphorus) release.


Raspberry Pi OS
---------------
Raspberry Pi OS stable, oldstable, and oldoldstable versions.


SLES
----
SLES versions through General Support. Versions in LTSS are not supported.

https://www.suse.com/lifecycle/


.. _solaris-supported:

Solaris
-------
Solaris version 10 as of August 2021. Solaris version 11 as of September 2021.


.. Note::
	  Python 3 support only. Python 2 unsupported may be found on `archive.repo.saltproject.io <https://archive.repo.saltproject.io/>`_.


The following systems are supported:

.. list-table::
   :widths: 50 50
   :header-rows: 1

   * - Device
     - Supported Firmware
   * - Solaris 10 update 8 and greater
     - Intel and SPARC processors
   * - Solaris 11.2 and greater
     - Intel and SPARC processors



Ubuntu
------
Ubuntu LTS versions through end-of-life. Ubuntu optional Extended Security
Maintenance (ESM) is not supported.

http://www.ubuntu.com/info/release-end-of-life


Windows
-------
Windows versions through Extended Support.

http://windows.microsoft.com/en-us/windows/lifecycle 




Reasonable-effort support policy by operating system
====================================================
This section outlines the general support policy for each operating system that
is listed as having reasonable-effort support by the Salt Project. These
guidelines are intended to help you understand how long a particular operating
system might receive reasonable-effort support.

Since the Salt Project does not create or maintain the packages for these
operating systems, no guarantee is made as to availability of packages. These
guidelines are for reasonable-effort support only.



FreeBSD
-------
FreeBSD versions through end-of-life.

https://www.freebsd.org/security/


openSUSE
--------
openSUSE version support mirrors that of the upstream maintainer.


SmartOS
-------
Latest version of SmartOS.
