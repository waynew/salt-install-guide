.. _announcements:

=============
Announcements
=============

.. |date| date::

.. epigraph::

    Last updated on |date|


Release announcements
=====================

* **June 21:** `3004.2 <https://docs.saltproject.io/en/3004/topics/releases/3004.2.html>`_ is now available.
* **June 21:** `3003.5 <https://docs.saltproject.io/en/3003/topics/releases/3003.5.html>`_ is now available.
* **June 21:** `3002.9 <https://docs.saltproject.io/en/3002/topics/releases/3002.9.html>`_ is now available.
* **June 21:** Salt Project has identified a CVE that needs immediate
  attention. See `Salt Security Advisory Release - June 21, 2022 <https://saltproject.io/security_announcements/salt-security-advisory-release-june-21st-2022/>`_ for more information.


Security announcements
======================

See `Security announcements <https://saltproject.io/security_announcements/>`_
for the latest announcements.

.. Important::
    To be notified of the latest security updates, subscribe to the
    `Salt security announcements RSS feed <https://saltproject.io/rss-feeds/>`_.
