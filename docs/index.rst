============
Install Salt
============

.. include:: topics/_includes/install-overview.rst


.. toctree::
   :maxdepth: 2
   :hidden:

   topics/overview
   topics/announcements
   topics/downloads

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Before you start

   topics/check-system-requirements
   topics/check-network-ports
   topics/install-salt-dependencies

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Install Salt

   topics/bootstrap
   topics/install-by-operating-system/index
   topics/platform-agnostic

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Post-installation

   topics/configure-master-minion
   topics/start-salt-services
   topics/accept-keys
   topics/verify-install
   topics/uninstall

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Upgrade Salt

   topics/upgrade
   topics/upgrade-to-onedir

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Support

   topics/salt-supported-operating-systems
   topics/salt-version-support-lifecycle
   topics/salt-python-version-support

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Contribute

   See a problem? Open an issue! <https://gitlab.com/saltstack/open/docs/salt-install-guide/-/issues>
   Salt docs contributing guide <https://saltstack.gitlab.io/open/docs/docs-hub/topics/contributing.html>
   GitLab repository <https://gitlab.com/saltstack/open/docs/salt-install-guide>
