.. |release| replace:: 3005
.. |release-badge| replace:: :bdg-link-success:`3005 <https://docs.saltproject.io/en/latest/topics/releases/index.html>`
.. |supported-release-1-badge| replace:: :bdg-link-primary:`3004.2 <https://docs.saltproject.io/en/3004/topics/releases/3004.2.html>`
.. |supported-release-2-badge| replace:: :bdg-link-info:`3003.5 <https://docs.saltproject.io/en/3003/topics/releases/3003.5.html>`
.. |juniper-file-version| replace:: 20210827-213932
.. |native-minion-python-version| replace:: Python 3.7.10

.. |amazon-linux2-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |amazon-linux2-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |amazon-linux2-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |amazon-linux2-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |amazon-linux2-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |amazon-linux2-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |amazon-linux2-latest-gpg| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-latest-download| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/latest.repo
.. |amazon-linux2-major-gpg| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/3004/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-major-download| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/3004.repo
.. |amazon-linux2-minor-gpg| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/archive/3004.1/SALTSTACK-GPG-KEY.pub
.. |amazon-linux2-minor-download| replace:: https://repo.saltproject.io/py3/amazon/2/x86_64/archive/3004.1.repo

.. |centos9-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos9-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos9-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos9-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos9-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos9-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |centos8-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos8-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos8-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos8-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos8-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos8-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |centos7-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos7-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos7-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos7-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos7-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |centos7-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |debian11-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |debian11-arm64-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |debian11-arm64-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |debian11-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/11/amd64/latest/salt-archive-keyring.gpg
.. |debian11-latest-download| replace:: https://repo.saltproject.io/py3/debian/11/amd64/latest bullseye main
.. |debian11-major-gpg| replace:: https://repo.saltproject.io/py3/debian/11/amd64/3004/salt-archive-keyring.gpg
.. |debian11-major-download| replace:: https://repo.saltproject.io/py3/debian/11/amd64/3004 bullseye main
.. |debian11-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/11/amd64/archive/3004.1/salt-archive-keyring.gpg
.. |debian11-minor-download| replace:: https://repo.saltproject.io/py3/debian/11/amd64/archive/3004.1 bullseye main

.. |debian11-arm64-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/11/arm64/latest/salt-archive-keyring.gpg
.. |debian11-arm64-latest-download| replace:: https://repo.saltproject.io/py3/debian/11/arm64/latest bullseye main
.. |debian11-arm64-major-gpg| replace:: https://repo.saltproject.io/py3/debian/11/arm64/3004/salt-archive-keyring.gpg
.. |debian11-arm64-major-download| replace:: https://repo.saltproject.io/py3/debian/11/arm64/3004 bullseye main
.. |debian11-arm64-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/11/arm64/archive/3004.1/salt-archive-keyring.gpg
.. |debian11-arm64-minor-download| replace:: https://repo.saltproject.io/py3/debian/11/arm64/archive/3004.1 bullseye main

.. |debian10-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/10/amd64/latest/salt-archive-keyring.gpg
.. |debian10-latest-download| replace:: https://repo.saltproject.io/py3/debian/10/amd64/latest buster main
.. |debian10-major-gpg| replace:: https://repo.saltproject.io/py3/debian/10/amd64/3004/salt-archive-keyring.gpg
.. |debian10-major-download| replace:: https://repo.saltproject.io/py3/debian/10/amd64/3004 buster main
.. |debian10-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/10/amd64/archive/3004.1/salt-archive-keyring.gpg
.. |debian10-minor-download| replace:: https://repo.saltproject.io/py3/debian/10/amd64/archive/3004.1 buster main

.. |debian9-latest-gpg| replace:: https://repo.saltproject.io/py3/debian/9/amd64/latest/salt-archive-keyring.gpg
.. |debian9-latest-download| replace:: https://repo.saltproject.io/py3/debian/9/amd64/latest stretch main
.. |debian9-major-gpg| replace:: https://repo.saltproject.io/py3/debian/9/amd64/3004/salt-archive-keyring.gpg
.. |debian9-major-download| replace:: https://repo.saltproject.io/py3/debian/9/amd64/3004 stretch main
.. |debian9-minor-gpg| replace:: https://repo.saltproject.io/py3/debian/9/amd64/archive/3004.1/salt-archive-keyring.gpg
.. |debian9-minor-download| replace:: https://repo.saltproject.io/py3/debian/9/amd64/archive/3004.1 stretch main

..
   IMPORTANT: macOS URLs need to be edited directly on the macOS downloads include when releasing. topics > _includes > macos-downloads.rst

.. |photonos3-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |photonos3-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |rhel9-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel9-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel9-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel9-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel9-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel9-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |rhel8-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel8-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel8-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel8-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel8-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel8-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |rhel8-latest-gpg| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |rhel8-latest-download| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/latest.repo
.. |rhel8-major-gpg| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/3004/SALTSTACK-GPG-KEY.pub
.. |rhel8-major-download| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/3004.repo
.. |rhel8-minor-gpg| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/archive/3004.1/SALTSTACK-GPG-KEY.pub
.. |rhel8-minor-download| replace:: https://repo.saltproject.io/py3/redhat/8/x86_64/archive/3004.1.repo

.. |rhel7-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel7-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel7-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel7-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel7-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |rhel7-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |rhel7-latest-gpg| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/latest/SALTSTACK-GPG-KEY.pub
.. |rhel7-latest-download| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/latest.repo
.. |rhel7-major-gpg| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/3004/SALTSTACK-GPG-KEY.pub
.. |rhel7-major-download| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/3004.repo
.. |rhel7-minor-gpg| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/archive/3004.1/SALTSTACK-GPG-KEY.pub
.. |rhel7-minor-download| replace:: https://repo.saltproject.io/py3/redhat/7/x86_64/archive/3004.1.repo

.. |ubuntu22-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |ubuntu22-arm64-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu22-arm64-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |ubuntu20-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |ubuntu20-latest-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/latest/salt-archive-keyring.gpg
.. |ubuntu20-latest-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/latest focal main
.. |ubuntu20-major-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3004/salt-archive-keyring.gpg
.. |ubuntu20-major-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/3004 focal main
.. |ubuntu20-minor-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/archive/3004.1/salt-archive-keyring.gpg
.. |ubuntu20-minor-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/amd64/archive/3004.1 focal main

.. |ubuntu20-arm64-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu20-arm64-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |ubuntu20-arm64-latest-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/latest/salt-archive-keyring.gpg
.. |ubuntu20-arm64-latest-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/latest focal main
.. |ubuntu20-arm64-major-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/3004/salt-archive-keyring.gpg
.. |ubuntu20-arm64-major-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/3004 focal main
.. |ubuntu20-arm64-minor-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/archive/3004.1/salt-archive-keyring.gpg
.. |ubuntu20-arm64-minor-download| replace:: https://repo.saltproject.io/py3/ubuntu/20.04/arm64/archive/3004.1 focal main

.. |ubuntu18-onedir-latest-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu18-onedir-latest-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu18-onedir-major-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu18-onedir-major-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu18-onedir-minor-gpg| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE
.. |ubuntu18-onedir-minor-download| replace:: LINK UNAVAILABLE UNTIL SALT 3005 (PHOSOPHORUS) RELEASE

.. |ubuntu18-latest-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/latest/salt-archive-keyring.gpg
.. |ubuntu18-latest-download| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/latest bionic main
.. |ubuntu18-major-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/3004/salt-archive-keyring.gpg
.. |ubuntu18-major-download| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/3004 bionic main
.. |ubuntu18-minor-gpg| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/archive/3004.1/salt-archive-keyring.gpg
.. |ubuntu18-minor-download| replace:: https://repo.saltproject.io/py3/ubuntu/18.04/amd64/archive/3004.1 bionic main

..
   IMPORTANT: Windows URLs need to be edited directly on the Windows downloads include when releasing. topics > _includes > windows-downloads.rst
